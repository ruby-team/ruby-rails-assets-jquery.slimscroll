# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-jquery.slimscroll/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-jquery.slimscroll"
  spec.version       = RailsAssetsJquerySlimscroll::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "slimScroll is a small jQuery plugin that transforms any div into a scrollable area. slimScroll doesn't occupy any visual space as it only appears on a user initiated mouse-over."
  spec.summary       = "slimScroll is a small jQuery plugin that transforms any div into a scrollable area. slimScroll doesn't occupy any visual space as it only appears on a user initiated mouse-over."
  spec.homepage      = "http://rocha.la/jQuery-slimScroll"

  spec.files         = `find ./* -type f | cut -b 3-`.split($/)
  spec.require_paths = ["lib"]

  spec.add_dependency "rails-assets-jquery", ">= 1.7"
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
